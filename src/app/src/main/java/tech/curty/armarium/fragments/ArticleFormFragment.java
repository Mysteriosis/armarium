package tech.curty.armarium.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import butterknife.BindView;
import tech.curty.armarium.R;
import tech.curty.armarium.activities.MainActivity;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ArticleFormFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ArticleFormFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleFormFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;

    private OnFragmentInteractionListener mListener;
    private MainActivity activity;

    @BindView(R.id.main_toolbar)
    protected Toolbar toolbar;

    private ImageView imageColor;

    public ArticleFormFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ArticleFormFragment.
     */
    public static ArticleFormFragment newInstance(String param1) {
        ArticleFormFragment fragment = new ArticleFormFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }

        activity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_article_infos, container, false);
        //activity.getDrawerToggle().setDrawerIndicatorEnabled(false);
        activity.getDrawerToggle().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        activity.getSupportActionBar().setTitle(R.string.new_article);

//        imageColor = (ImageView) rootView.findViewById(R.id.img_articleColor);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.search_article);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        Spinner spinner = (Spinner) rootView.findViewById(R.id.types_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(rootView.getContext(),
                R.array.articles_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

//        final HSLColorPicker colorPicker = (HSLColorPicker) rootView.findViewById(R.id.articleColorPicker);
//        colorPicker.setColorSelectionListener(new SimpleColorSelectionListener() {
//            @Override
//            public void onColorSelected(int color) {
//                // Do whatever you want with the color
//                updateImage(color, PorterDuff.Mode.MULTIPLY);
//
//            }
//        });

        return rootView;
    }

    private void updateImage(int color, PorterDuff.Mode mode) {
        imageColor.getBackground().setColorFilter(color, mode);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
