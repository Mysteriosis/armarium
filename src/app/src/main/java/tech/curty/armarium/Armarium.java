package tech.curty.armarium;

import android.app.Application;
import net.danlew.android.joda.JodaTimeAndroid;

public class Armarium extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }
}