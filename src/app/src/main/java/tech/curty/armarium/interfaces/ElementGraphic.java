package tech.curty.armarium.interfaces;

/**
 * Created by Pierre-Alain Curty on 09.01.2018.
 */

public interface ElementGraphic {
    String getValue();
}
