package tech.curty.armarium.adapter;

/**
 * Created by Pierre-Alain Curty on 01.01.2018.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.MutableArray;
import com.couchbase.lite.MutableDictionary;
import com.couchbase.lite.MutableDocument;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tech.curty.armarium.R;
import tech.curty.armarium.activities.MainActivity;
import tech.curty.armarium.data.DatabaseManager;
import tech.curty.armarium.fragments.ClosetFragment;
import tech.curty.armarium.model.Article;

public class ArticleCardAdapter extends RecyclerView.Adapter<ArticleCardAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder{
        public AppCompatImageView image = null;
        public AppCompatImageView submenu = null;
        public AppCompatTextView name = null;
        public AppCompatTextView type = null;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.card_article_thumbnail);
            submenu = itemView.findViewById(R.id.card_article_overflow);
            name = itemView.findViewById(R.id.card_article_name);
            type = itemView.findViewById(R.id.card_article_type);
        }
    }

    private ArrayList<Article> mArticles = new ArrayList<>();
    private Context mContext;

    public ArticleCardAdapter(Context context, ArrayList<Article> data) {
        mContext = context;
        mArticles = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.article_card, parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(mArticles != null) {
            Article article = mArticles.get(position);

            //Render image using Picasso library
            if (!TextUtils.isEmpty(article.getThumbnailURL())) {
                Picasso.with(mContext).load(article.getThumbnailURL())
                        .error(R.drawable.ic_menu_camera)
                        .placeholder(R.drawable.ic_menu_camera)
                        .into(holder.image);
            }

            holder.name.setText(article.getName());
            holder.type.setText(article.getType());
            holder.submenu.setTag(article);
            holder.submenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopupMenu(holder.submenu);
                }
            });
        }
    }

    // 3 vertical dots menu
    private void showPopupMenu(final View view) {
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_article, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_edit_article:
                        Toast.makeText(mContext, ((Article)view.getTag()).getLocal_id(), Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.action_remove_article:
                        // Get profile
                        // TODO: Implement multiple profiles
                        Database database = DatabaseManager.getDatabase();
                        Document doc = database.getDocument(view.getContext().getString(R.string.id_profile));
                        Article a = (Article)view.getTag();

                        if(doc != null) {
                            MutableDocument mDoc = doc.toMutable();
                            MutableArray articles = mDoc.getArray("articles").toMutable();
                            for (int i = 0; i < articles.count(); i++) {
                                MutableDictionary md = articles.getDictionary(i);
                                if(md != null) {
                                    if (md.getString("name").equals(a.getName())) {
                                        mDoc.getArray("articles").remove(i);
                                        break;
                                    }
                                }
                            }
                            try {
                                database.save(mDoc);
                                ((MainActivity)mContext).reloadFragment();
                                Toast.makeText(mContext, "Article removed.", Toast.LENGTH_SHORT).show();
                            } catch (CouchbaseLiteException e) {
                                Log.e("COUCHBASE", "Couldn't modify profile");
                                e.printStackTrace();
                            }
                        }
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

    @Override
    public int getItemCount() {
        if(mArticles != null) {
            return mArticles.size();
        }
        return 0;
    }
}
