package tech.curty.armarium.data;

/**
 * Created by Pierre-Alain Curty on 17.01.2018.
 */

public interface SyncListener {
    void call();
}
