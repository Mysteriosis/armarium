package tech.curty.armarium.activities;

import butterknife.BindView;
import butterknife.ButterKnife;
import tech.curty.armarium.R;
import tech.curty.armarium.adapter.ArticleListAdapter;
import tech.curty.armarium.model.Article;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.MenuItem;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity {
    private final String REQUEST_TAG = "COUCHBASE REST API";
    private final String URL = "http://intra.curty.tech:8093/query/service";

    private ArticleListAdapter adapter;
    private RequestQueue requestQueue;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.search_article:
                        return true;
                    case R.id.search_outfit:
                        Snackbar.make(findViewById(R.id.search_container), "Should provide options to search / create an outfit", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        return true;
                    default:
                        return false;
                }
            }
        };

    @BindView(R.id.search_text)
    protected SearchView searchView;
    @BindView(R.id.search_result)
    protected RecyclerView recyclerView;
    @BindView(R.id.search_toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.search_empty)
    protected AppCompatTextView emptyText;
    @BindView(R.id.search_progress_bar)
    protected ProgressBar progressBar;

    private ArrayList<Article> articlesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        // DATA
        adapter = new ArticleListAdapter(this, null);
        requestQueue = Volley.newRequestQueue(this);

        // TOOLBAR
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setTitle(R.string.search_article_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        catch (NullPointerException e) {
            Log.e("SearchToolbar", "Nullpointer Exception when accessing SupportActionBar.");
        }

        searchView.onActionViewExpanded();
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                final Gson gson = new GsonBuilder().registerTypeAdapter(Article.class, new Article.ArticleDeserializer()).create();
                searchView.clearFocus();

                String text = (query.length() <= 5 ) ? query : query.substring(0,5);

                Map<String, String> jsonParams = new HashMap<>();
                jsonParams.put("statement", getString(R.string.query_search_articles));
                jsonParams.put("$key", "%" + text + "%");

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, new JSONObject(jsonParams),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            articlesList.clear();
                            try {
                                JSONArray array = response.getJSONArray("results");

                                if(array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        Article a = gson.fromJson(array.getJSONObject(i).getString(getString(R.string.articles_bucket_name)), Article.class);
                                        a.setSource(new JsonParser().parse(array.getJSONObject(i).getString(getString(R.string.articles_bucket_name))).getAsJsonObject(), array.getJSONObject(i).getString("id"));
                                        articlesList.add(a);
                                    }
                                    adapter = new ArticleListAdapter(SearchActivity.this, articlesList);
                                    SearchActivity.this.recyclerView.setAdapter(adapter);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyText.setVisibility(View.GONE);
                                }
                                else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyText.setVisibility(View.VISIBLE);
                                }
                            }
                            catch (JSONException jsonE){
                                Log.e(REQUEST_TAG, jsonE.toString());
                            }
                            SearchActivity.this.progressBar.setVisibility(View.GONE);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Snackbar.make(findViewById(R.id.search_container), "An error occurred.", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            Log.e(REQUEST_TAG, error.toString());
                            SearchActivity.this.progressBar.setVisibility(View.GONE);
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            String credentials = getString(R.string.db_user)+":"+getString(R.string.db_password);
                            String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                            headers.put("Authorization", auth);
                            return headers;
                        }
                    };
                requestQueue.add(jsonObjectRequest);

                SearchActivity.this.progressBar.setVisibility(View.VISIBLE);
                return true;
            }

            public boolean onQueryTextChange(String s) {
                if(s == null || s.isEmpty()) {
                    articlesList.clear();
                    adapter = new ArticleListAdapter(SearchActivity.this, null);
                    recyclerView.setAdapter(adapter);
                    return true;
                }
                return false;
            }
        });

        // CONTENT
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // BOTTOM
        BottomNavigationView navigation = findViewById(R.id.search_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void startRecognition(View view) {
        Intent i = new Intent(this, CaptureActivity.class);
        startActivityForResult(i, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("keyword");
                searchView.setQuery(result, false);
                searchView.setIconifiedByDefault(false);
                searchView.requestFocus();
            }
            if (resultCode == RESULT_CANCELED) {
                Snackbar.make(findViewById(R.id.search_container), "Can't process result", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }
}
