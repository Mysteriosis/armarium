package tech.curty.armarium.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.joda.time.DateTime;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by Pierre-Alain Curty on 01.01.2018.
 */

public class Article {
    private static final String TAG_ARTICLE = "Model Article";

    private class ArticleMedia {
        String name;
        String url;
        String source;

        public ArticleMedia(JsonObject media, boolean useSource){
            if (useSource) {
                this.name = media.getAsJsonObject("infos").get("id").getAsString();
                this.url = media.get("url").getAsString();
            }
            else {
                this.name = media.get("name").getAsString();
                this.url = media.get("path").getAsString();
                this.source = media.get("source").getAsString();
            }
        }

        public ArticleMedia(String url) {
            this.url = url;
        }

        public ArticleMedia(String url, String name) {
            this.name = name;
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPath() {
            return url;
        }

        public void setPath(String path) {
            this.url = path;
        }
    }

    public static class ArticleDeserializer implements JsonDeserializer<Article> {
        @Override
        public Article deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jobject = json.getAsJsonObject();
            Article a = null;

            // Test for missing type (fake article)
            JsonObject infos = jobject.getAsJsonObject("infos");
            JsonElement jo = infos.get("type");
            if(jo == null || jo.isJsonNull()) {
                String name = jobject.get("name").getAsString();
                String[] words = name.split(" ");
                String tempType = words[words.length-1];
                a = new Article(name, tempType);
            }
            else {
                a = new Article(
                    jobject.get("name").getAsString(),
                    jo.getAsString()
                );
            }

            a.setBrand(infos.get("brand").getAsString());
            a.setColor(infos.get("color").getAsString());
            a.setGenders(infos.get("genders").getAsString());
            a.setAge_groups(infos.get("age_groups").getAsString());

            JsonElement lid = jobject.get("local_id");
            if(lid != null) {
                a.setLocal_id(jobject.get("local_id").getAsString());
            }

            a.setSeason(jobject.get("season").getAsString());
            a.setCreated_at(jobject.get("created_at").getAsString());

            // Test for missing media (fake article)
            JsonArray ja = jobject.getAsJsonArray("media");
            if(ja == null) {
                a.addMedia(jobject.getAsJsonObject("source"), true);
            }
            else {
                for (JsonElement je : ja) {
                    a.addMedia(je.getAsJsonObject(), false);
                }
            }

            return a;
        }
    }

    private String local_id = null;
    private String _id = null;
    private String name = null;
    private String brand = null;
    private String type = null;
    private String color = null;
    //    private ArrayList<String> genders = null;
    private String genders = null;
    private String age_groups = null;
    private String season = null;
    private ArrayList<ArticleMedia> medias = new ArrayList<>();
    private DateTime created_at = null;

    private JsonObject source;

    public Article(String name, String type) {
        this.local_id = UUID.randomUUID().toString();
        this.name = name;
        this.type = type;
    }
    public Article(String name, String type, ArrayList<ArticleMedia> medias) {
        this(name, type);
        this.medias = medias;
    }

    public String getJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public String getLocal_id() {
        return local_id;
    }

    private void setLocal_id(String local_id) {
        this.local_id = local_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

//    public ArrayList<String> getGenders() {
//        return genders;
//    }
//
//    public void setGenders(ArrayList<String> genders) {
//        this.genders = genders;
//    }

    public String getGenders() {
        return genders;
    }

    public void setGenders(String genders) {
        this.genders = genders;
    }

    public String getAge_groups() {
        return age_groups;
    }

    public void setAge_groups(String age_groups) {
        this.age_groups = age_groups;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Collection<ArticleMedia> getMedias() {
        return medias;
    }

    public String getThumbnailURL() {
        if(this.medias != null)
            return ((ArticleMedia)medias.toArray()[0]).getPath();
        else
            return "";
    }

    public void setMedias(ArrayList<ArticleMedia> medias) {
        this.medias = medias;
    }
    public void addMedia(JsonObject source, boolean useSource) {
        this.medias.add(new ArticleMedia(source, useSource));
    }

    public DateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(DateTime created_at) {
        this.created_at = created_at;
    }
    public void setCreated_at(String created_at) {
        this.created_at = new DateTime(created_at);
    }

    public JsonObject getSource() {
        return source;
    }

    public JsonObject getSourceWithID() {
        source.addProperty("local_id", this.local_id);
        return source;
    }

    public void setSource(JsonObject json, String id) {
        if(id == null) {
            this.source = null;
        }
        else {
            JsonObject src = json;
            src.addProperty("_id", id);
            this.source = json;
        }
    }
}