package tech.curty.armarium.adapter;

/**
 * Created by Pierre-Alain Curty on 01.01.2018.
 */

import tech.curty.armarium.R;
import tech.curty.armarium.data.DatabaseManager;
import tech.curty.armarium.fragments.ClosetFragment;
import tech.curty.armarium.model.Article;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.MutableDocument;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ViewHolder> {

    private ArrayList<Article> mArticles;
    private Context mContext;
    private Database database;

    public class ViewHolder extends RecyclerView.ViewHolder{
        public AppCompatImageView image;
        public AppCompatTextView name;
        public AppCompatTextView type;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.list_article_thumbnail);
            name = itemView.findViewById(R.id.list_article_name);
            type = itemView.findViewById(R.id.list_article_type);

            itemView.setOnClickListener(clickListener);
        }

        private View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Document profile = database.getDocument(v.getContext().getString(R.string.id_profile));
                if(profile != null) {
                    MutableDocument doc = profile.toMutable();
                    MutableDocument mDoc = null;

                    if(doc != null) {
                        Gson gson = new Gson();
                        mDoc = doc.toMutable();
                        Article a = (Article)v.getTag();
                        Map map = gson.fromJson(a.getSource(), Map.class);
                        mDoc.getArray("articles").addValue(map);

                        try {
                            database.save(mDoc);
                            ((Activity)mContext).setResult(ClosetFragment.SEARCH_TAG);
                            ((Activity)mContext).finish();
                        } catch (CouchbaseLiteException e) {
                            Log.e("COUCHBASE", "Couldn't modify profile");
                            e.printStackTrace();
                        }
                    }
                }
                else
                    Toast.makeText(mContext, "Can't load profile", Toast.LENGTH_LONG).show();
            }
        };
    }

    public ArticleListAdapter(Context context, ArrayList<Article> data) {
        mContext = context;
        mArticles = data;
        database = DatabaseManager.getDatabase();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.article_row, parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(mArticles != null) {
            Article article = mArticles.get(position);

            //Render image using Picasso library
            if (!TextUtils.isEmpty(article.getThumbnailURL())) {
                Picasso.with(mContext).load(article.getThumbnailURL())
                        .error(R.drawable.ic_menu_camera)
                        .placeholder(R.drawable.ic_menu_camera)
                        .into(holder.image);
            }
            holder.name.setText(article.getName());
            holder.type.setText(article.getType());
            holder.itemView.setTag(article);
        }
    }

    @Override
    public int getItemCount() {
        if(mArticles != null) {
            return mArticles.size();
        }
        return 0;
    }
}
