package tech.curty.armarium.data;

/**
 * Created by Pierre-Alain Curty on 01.01.2018.
 */

import android.content.Context;
import android.util.Log;

import com.couchbase.lite.BasicAuthenticator;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseConfiguration;
import com.couchbase.lite.IndexBuilder;
import com.couchbase.lite.Replicator;
import com.couchbase.lite.ReplicatorChange;
import com.couchbase.lite.ReplicatorChangeListener;
import com.couchbase.lite.ReplicatorConfiguration;
import com.couchbase.lite.URLEndpoint;
import com.couchbase.lite.ValueIndexItem;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class DatabaseManager {
    private final static String DATABASE_NAME = "profiles";
    private final static String SYNC_GATEWAY = "ws://intra.curty.tech:4984/";

    private static DatabaseManager instance = null;
    private static Database database = null;
    private static ArrayList<SyncListener> listeners = null;

    public static DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
        return instance;
    }

    private DatabaseManager(Context context) {
        listeners = new ArrayList<>();

        // Set Database configuration
        DatabaseConfiguration config = new DatabaseConfiguration(context);

        try {
            // Create / Open a database with specified name and configuration
            database = new Database(DATABASE_NAME, config);
            // Create an index on profile's type
            database.createIndex("emailIndex", IndexBuilder.valueIndex(ValueIndexItem.property("email")));
        }
        catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public static void startPushAndPullReplication(Context context, int username, int password) {
        startPushAndPullReplication(context.getString(username), context.getString(password));
    }

    public static void addListener(SyncListener sl) {
        listeners.add(sl);
    }

    public static void removeListener(SyncListener sl) {
        listeners.remove(sl);
    }

    public static void startPushAndPullReplication(String username, String password) {
        try {
            URI url = new URI(SYNC_GATEWAY + "/" + DATABASE_NAME);

            ReplicatorConfiguration config = new ReplicatorConfiguration(database, new URLEndpoint(url));
            config.setReplicatorType(ReplicatorConfiguration.ReplicatorType.PUSH_AND_PULL);
            config.setContinuous(true);
            config.setAuthenticator(new BasicAuthenticator(username, password));

            Replicator replicator = new Replicator(config);
            replicator.addChangeListener(new ReplicatorChangeListener() {
                @Override
                public void changed(ReplicatorChange change) {
                    if (change.getReplicator().getStatus().getActivityLevel().equals(Replicator.ActivityLevel.IDLE)) {
                        for(SyncListener listener : listeners) {
                            try {
                                listener.call();
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Log.e("COUCHBASE", "Schedular Completed");
                    }
                    if (change.getReplicator().getStatus().getActivityLevel().equals(Replicator.ActivityLevel.STOPPED)
                            || change.getReplicator().getStatus().getActivityLevel().equals(Replicator.ActivityLevel.OFFLINE)) {
                        // stopReplication();
                        Log.e("COUCHBASE", "ReplicationTag Stopped");
                    }
                }
            });
            replicator.start();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static Database getDatabase() {
        return database;
    }
}
