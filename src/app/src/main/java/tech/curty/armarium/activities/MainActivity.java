package tech.curty.armarium.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;

import butterknife.BindView;
import butterknife.ButterKnife;
import tech.curty.armarium.data.DatabaseManager;
import tech.curty.armarium.fragments.ArticleFormFragment;
import tech.curty.armarium.fragments.CalendarFragment;
import tech.curty.armarium.fragments.ClosetFragment;
import tech.curty.armarium.fragments.OutfitsFragment;
import tech.curty.armarium.R;
import tech.curty.armarium.fragments.SettingsFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ArticleFormFragment.OnFragmentInteractionListener,
        ClosetFragment.OnFragmentInteractionListener,
        OutfitsFragment.OnFragmentInteractionListener,
        CalendarFragment.OnFragmentInteractionListener,
        SettingsFragment.OnFragmentInteractionListener {

    static private final String FRAG_TAG = "SwapFragment";


    @BindView(R.id.main_toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawer;

    private ActionBarDrawerToggle drawerToggle;
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setupDrawerContent(navigationView);

        // START COUCHBASE
        DatabaseManager.getInstance(this);
        DatabaseManager.startPushAndPullReplication(this, R.string.db_user, R.string.db_password);

        changeFragment(ClosetFragment.class, false, false);
    }

    // TOOLBAR

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                changeFragment(SettingsFragment.class, false, true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // DRAWER

    /**
     * Change the current displayed fragment by a new one.
     * - if the fragment is in backstack, it will pop it
     * - if the fragment is already displayed (trying to change the fragment with the same), it will not do anything
     *
     * @param fragmentClass   the class of the new fragment to display
     * @param saveInBackstack if we want the fragment to be in backstack
     * @param animate         if we want a nice animation or not
     */
    protected void changeFragment(Class fragmentClass, boolean saveInBackstack, boolean animate) {
        Fragment frag = null;

        try {
            frag = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        currentFragment = frag;
        String backStateName = ((Object) frag).getClass().getName();

        try {
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
                //fragment not in back stack, create it.
                FragmentTransaction transaction = manager.beginTransaction();

                if (animate) {
                    Log.d(FRAG_TAG, "Change Fragment: animate");
                    transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }

                transaction.replace(R.id.app_content, frag, backStateName);

                if (saveInBackstack) {
                    Log.d(FRAG_TAG, "Change Fragment: addToBackTack " + backStateName);
                    transaction.addToBackStack(backStateName);
                } else {
                    Log.d(FRAG_TAG, "Change Fragment: NO addToBackStack");
                }

                transaction.commit();
            } else {
                // custom effect if fragment is already instanciated
            }
        } catch (IllegalStateException exception) {
            Log.w(FRAG_TAG, "Unable to commit fragment, could be activity as been killed in background. " + exception.toString());
        }
    }

    public void reloadFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.detach(currentFragment).attach(currentFragment).commit();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener( new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Class fragmentClass = null;
                int id = menuItem.getItemId();

                if (id == R.id.nav_closet) {
                    fragmentClass = ClosetFragment.class;
                } else if (id == R.id.nav_outfits) {
                    fragmentClass = OutfitsFragment.class;
                } else if (id == R.id.nav_calendar) {
                    fragmentClass = CalendarFragment.class;
                } else if (id == R.id.nav_manage) {
                    fragmentClass = SettingsFragment.class;
                }

                changeFragment(fragmentClass, false, true);

                // Highlight the selected item has been done by NavigationView
                menuItem.setChecked(true);
                // Set action bar title
                setTitle(menuItem.getTitle());
                // Close the search_navigation drawer
                drawer.closeDrawers();
                return true;
            }
        });
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // Handle search_navigation view item clicks here.
        int id = menuItem.getItemId();

        if (id == R.id.nav_closet) {
            // Handle the camera action
        } else if (id == R.id.nav_outfits) {

        } else if (id == R.id.nav_calendar) {

        } else if (id == R.id.nav_manage) {

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.w("TEST", "Fragment interaction !!!");
    }

    public ActionBarDrawerToggle getDrawerToggle() {
        return drawerToggle;
    }
}
